from django.db import models
from django.forms import ModelForm

# Create your models here.
class Jadwal(models.Model):
    mata_kuliah = models.CharField(blank=False, max_length= 100)
    dosen_pengajar = models.CharField(blank=False, max_length= 100)
    jumlah_sks = models.CharField(blank=False, max_length= 100)
    semester_tahun = models.CharField(blank=False, max_length= 100)
    ruang_kelas = models.CharField(blank=False, max_length= 100)
    deskripsi = models.CharField(blank=False, max_length= 100)
