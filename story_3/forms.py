from django import forms

class formulir(forms.Form):
    mata_kuliah = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Mata Kuliah',
        'type' : 'text',
        'required' : True
    }))
    dosen_pengajar = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Dosen Pengajar',
        'type' : 'text',
        'required' : True
    }))
    jumlah_sks = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Jumlah SKS',
        'type' : 'text',
        'required' : True
    }))
    semester_tahun = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Semester dan Tahun',
        'type' : 'text',
        'required' : True
    }))
    ruang_kelas = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Ruang Kelas',
        'type' : 'text',
        'required' : True
    }))
    deskripsi = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Deskripsi Mata Kuliah',
        'type' : 'text',
        'required' : True
    }))