from django.shortcuts import render, redirect
from .models import Jadwal
from .forms import formulir
import datetime

# Create your views here.
def index(request):
    return render(request, 'index.html')

def CV_page(request):
    return render(request, 'CV_page.html')

def time(request, timeChange):
    timeChange = int(timeChange)
    time = datetime.datetime.now() + datetime.timedelta(hours=timeChange)
    return render(request, 'time.html', {"finalTime" : time })

def jadwal(request):
    if(request.method == "POST"):
        tmp = formulir(request.POST)
        if(tmp.is_valid()):
            tmp2 = Jadwal()
            tmp2.nama_kegiatan = tmp.cleaned_data["mata_kuliah"]
            tmp2.jumlah_sks = tmp.cleaned_data["jumlah_sks"]
            tmp2.semester_tahun = tmp.cleaned_data["semester_tahun"]
            tmp2.dosen_pengajar = tmp.cleaned_data["dosen_pengajar"]
            tmp2.ruang_kelas = tmp.cleaned_data["ruang_kelas"]
            tmp2.deskripsi = tmp.cleaned_data["deskripsi"]
            tmp2.save()
        return redirect("/form")
    else:
        tmp = formulir()
        tmp2 = Jadwal.objects.all()
        tmp_dictio = {
            'formulir' : tmp,
            'jadwal' : tmp2
        }
        return render(request, 'form.html', tmp_dictio)

def delete(request, pk):
    if(request.method == "POST"):
        tmp = formulir(request.POST)
        if(tmp.is_valid()):
            tmp2 = Jadwal()
            tmp2.nama_kegiatan = tmp.cleaned_data["mata_kuliah"]
            tmp2.jumlah_sks = tmp.cleaned_data["jumlah_sks"]
            tmp2.semester_tahun = tmp.cleaned_data["semester_tahun"]
            tmp2.dosen_pengajar = tmp.cleaned_data["dosen_pengajar"]
            tmp2.ruang_kelas = tmp.cleaned_data["ruang_kelas"]
            tmp2.deskripsi = tmp.cleaned_data["deskripsi"]
            tmp2.save()
        return redirect("/form")
    else:
        Jadwal.objects.filter(pk = pk).delete()
        tmp = formulir()
        tmp2 = Jadwal.objects.all()
        tmp_dictio = {
            'formulir' : tmp,
            'jadwal' : tmp2
        }
        return render(request, 'form.html', tmp_dictio)

