from django.shortcuts import render, redirect
from .models import Schedule, ScheduleForm

# Create your views here.
def index(request):
    return render(request, 'index.html')

def CV_page(request):
    return render(request, "CV_page.html")


def form(request):
    if request.method == "POST":
        schedform = ScheduleForm(request.POST)

        if schedform.is_valid():
            schedform.save()
        
            return redirect("story_3:index")

    else:
        schedform = ScheduleForm()

    return render(request, "form.html", {'schedform':schedform})
            

