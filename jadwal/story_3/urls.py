from django.urls import path
from . import views

app_name = 'story_3'

urlpatterns = [
    path('', views.index, name='index'),
    path('CV', views.CV_page, name='CV_page'),
    # dilanjutkan ...
]
